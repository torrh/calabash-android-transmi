Feature: Opening the viajes

  Scenario: As a user I want to be able to open viajar en transmilenio
    Given I press "Paraderos"
    #button to remove the splash screen
    Then I press "Viajar en Transmi, SITP o Taxi"
    And I wait
    Then I press "Punto de origen"
    Then I should see "Trayectos recientes"

  Scenario: As a user I want to be able to open viajar en transmilenio and click on taxi button
    Given I press "Viajar en Transmi, SITP o Taxi"
    And I wait
    Then I press "Taxi"
    Then I should see "Inicia sesión con Facebook"
    Then I go back

  Scenario: As a user I want to be able to open viajar en transmilenio and click on mapa sistema
    Given I press "Viajar en Transmi, SITP o Taxi"
    And I wait
    Then I press "Punto de destino"
    Then I press "AÑADIR"
    Then I enter "Casa" into input field number 1
    Then I enter "Carrera 100" into input field number 2
    Then I go back
